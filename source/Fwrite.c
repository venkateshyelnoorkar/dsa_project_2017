#include"File.h"

size_t Fwrite(void *ptr, size_t size, size_t nmemb, File *fp){
  char *temp;//to access the byte by byte memory pointed to by the ptr
  temp = ptr;
  size_t total = size * nmemb;
  size_t net_write = 0;
  if(!(fp->flag & Write)){

    return 0;//error invalid mode
  }

  if((fp->count == 0) || (fp->ptr == NULL)){
    Flushbuf(fp);
  }
  /*//if fp->count is zero after Fillbuf i.e EOF has occured
  if(fp->count == 0)
    return net_write;
*/

  while((fp->count) && (net_write < total)){
    *(fp->ptr) = *temp;
    temp++;
    fp->ptr++;
    fp->count--;
  //  printf("%d  fp->count\n", fp->count);
    net_write++;
    if((fp->count == 0) || (fp->ptr == NULL)){
      Flushbuf(fp);
    }
  //  if(fp->count == 0)
    //  return net_write;

  }

  return net_write;

}
