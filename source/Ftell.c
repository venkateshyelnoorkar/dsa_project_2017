#include"File.h"
long Ftell(File *fp){

  clearbuf(fp);

  long x;
  x = lseek(fp->fd, 0L, 1);
  //printf("this is the x from ftell %d\n", x);
  if(x > (MAX_BUF - 1)){
    //printf("x>max buf   %d\n", x);
    return (x  - fp->count );
  }
  return x - fp->count ;
}
