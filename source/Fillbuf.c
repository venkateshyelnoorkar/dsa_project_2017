#include"File.h"
void Fillbuf(File *fp){
    int buf_size;
    if(fp->flag & Unbuf)
      buf_size = 1;
    else
      buf_size = MAX_BUF;
  printf("FILLBUF CALLED\n");
    fp->ptr = fp->buffer;
    fp->count = read(fp->fd, fp->buffer, buf_size);

    if(fp->count == 0)
      fp->flag |= Eof;

    if(fp->count < 0){
      fp->flag |= Err;
      fp->count = 0;
      return;
    }


}
