#include"File.h"

size_t Fread(void *ptr, size_t size, size_t nmemb, File *fp){
  char *temp;//to access the byte by byte memory pointed to by the ptr
  temp = ptr;
  size_t total = size * nmemb;
  size_t net_read = 0;
  if(!(fp->flag & Read)){
  //  printf("flag not read\n");
    return 0;//error invalid mode
  }

  if(fp->count == 0){
  //  printf("gone\n");
    Fillbuf(fp);
  }
  //if fp->count is zero after Fillbuf i.e EOF has occured
  if(fp->count == 0)
    return net_read;


  while((fp->count) && (net_read < total)){
    *temp = *(fp->ptr);
    temp++;
    fp->ptr++;
    fp->count--;
    net_read++;
    if(fp->count == 0){
      Fillbuf(fp);
    }
    if(fp->count == 0)
      return net_read;

  }

  return 0;

}
