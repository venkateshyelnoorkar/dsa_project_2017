#include"File.h"
 int Fseek(File *fp, long offset, int whence){
   if(whence == SEEK_SET){
     if(lseek(fp->fd, offset, 0) != -1){
       fp->ptr = NULL;
       fp->count = 0;
       return 0;
     }
     else{
       fp->flag |= Err;
       return -1;
     }
   }

   if(whence == SEEK_END){
     if(lseek(fp->fd, offset, 2) != -1){
       fp->ptr = NULL;
       fp->count = 0;
       return 0;
     }
     else{
       fp->flag |= Err;
       return -1;
     }
   }

   if(whence == SEEK_CUR){
     if(fp->flag & Write)
      clearbuf(fp);

    long x;
    x = lseek(fp->fd, 0L, 1 );
      //printf("this is the x from ftell %d\n", x);
    if(x > (MAX_BUF - 1)){
      if(lseek(fp->fd, offset - fp->count, 1) != -1){
        fp->ptr = NULL;
        fp->count = 0;
        return 0;
      }
      else{
        fp->flag |= Err;
        return -1;
      }

    }

    else{
      if(lseek(fp->fd, offset - fp->count, 1) != -1){
        fp->ptr = NULL;
        fp->count = 0;
        return 0;
      }
      else{
        fp->flag |= Err;
        return -1;
      }
    }


   }

   fp->flag |= Err;
   return -1;
  //   return (x  - fp->count );


 }
