#include"File.h"
void clearbuf(File *fp){

  if(fp->flag & Write){

    int buf_size;
    if(fp->flag & Unbuf)
      buf_size = 1;
    else
      buf_size = MAX_BUF;
    if(fp->ptr > fp->buffer)
      write(fp->fd, fp->buffer, (buf_size - fp->count));
    fp->ptr = NULL;
    fp->count = 0;
  }
}
