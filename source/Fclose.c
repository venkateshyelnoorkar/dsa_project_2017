#include"File.h"


int Fclose(File *fp){

  clearbuf(fp);

  if(close(fp->fd) == -1)
    return EOF;
  free(fp);
  file_count --;
  return 0;

}
