#include"File.h"
void Flushbuf(File *fp){
  //  printf("Flushbuf CALLED\n");
    int flag, buf_size;
    if(fp->flag & Unbuf)
      buf_size = 1;
    else
      buf_size = MAX_BUF;
    if(fp->ptr != NULL){
    //  printf("fp->ptr != NULL *****\n");
      flag = write(fp->fd, fp->buffer, buf_size);
  //    printf("%d\n", flag);
      if(flag == -1){
      //  printf("error **********\n");
        fp->flag |= Err;
        return;
      }

    }

    fp->ptr = fp->buffer;
    fp->count = buf_size;
    return;



}
