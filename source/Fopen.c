#include"File.h"

#define PERMS 0777 //read write execute for everyone

int file_count = 0;
//Fopen: opens the file and returns File pointer
File *Fopen(char *name, char *mode){
  File *fp;
  int fd;

  // handling the wrong mode condition
    //*******************************************
  //print invalid argument

//**************************************************
  //  finding free file slot in the file_arr
  /*for(fp = file_arr; fp < (file_arr + MAX_FILE); fp++){
    if(!(fp->flag)) // found a slot for the current file in file_arr
      break;
  }
*/

  if(file_count < MAX_FILE){
    fp = malloc(sizeof(File));
    if(fp == NULL)
      return NULL;
    file_count++;
    fp->ptr = NULL;
    fp->count = 0;
    fp->flag = 0;
  }


  //no free slots found in the file_arr
  //if(fp >= (file_arr + MAX_FILE)){
    //*************************************************
    //print error : max number of files openi

    //**************************************************
    //return NULL;
  //}

  //when mode is w
  if(!(strcmp(mode, "w"))){
    fd  = creat(name, PERMS);
    fd = open(name, O_WRONLY, PERMS);
    if(fd == -1)
      return NULL;
    fp->fd = fd;
    fp->flag |= Write;
  }

  //when mode is r
  if(!(strcmp(mode, "r"))){
    fd  = open(name, O_CREAT | O_RDONLY, PERMS);
    if(fd == -1)
      return NULL;
    fp->fd = fd;
    fp->flag |= Read;
  }


  //when mode is a
  if(!(strcmp(mode, "a"))){
    fd  = open(name, O_CREAT | O_WRONLY, PERMS);
    if(fd == -1)
      return NULL;
    lseek(fd, 0L, 2);
    fp->fd = fd;
    fp->flag |= Write;
  }

  //when mode is wb
  if(!(strcmp(mode, "wb"))){
    fd  = creat(name, PERMS);
    fd = open(name, O_WRONLY, PERMS);
    if(fd == -1)
      return NULL;
    fp->fd = fd;
    fp->flag |= Write;
  }

  //when mode is rb
  if(!(strcmp(mode, "rb"))){
    fd  = open(name, O_CREAT | O_RDONLY, PERMS);
    if(fd == -1)
      return NULL;
    fp->fd = fd;
    fp->flag |= Read;
  }


  //when mode is ab
  if(!(strcmp(mode, "ab"))){
    fd  = open(name, O_CREAT | O_WRONLY, PERMS);
    if(fd == -1)
      return NULL;
    lseek(fd, 0L, 2);
    fp->fd = fd;
    fp->flag |= Write;
  }



  //when mode is w+
  if(!(strcmp(mode, "w+"))){
    fd  = creat(name, PERMS);
    fd  = open(name, O_CREAT | O_RDWR, PERMS);


    if(fd == -1)
      return NULL;
    fp->fd = fd;
    fp->flag |= (Write | Read | Unbuf);

  }


  //when mode is r+
  if(!(strcmp(mode, "r+"))){
    fd  = open(name, O_CREAT | O_RDWR, PERMS);
    if(fd == -1)
      return NULL;
    fp->fd = fd;
    fp->flag |= (Write | Read | Unbuf);
  }

  //when mode is a+
  if(!(strcmp(mode, "a+"))){
    fd  = open(name, O_CREAT | O_RDWR, PERMS);
    if(fd == -1)
      return NULL;
    lseek(fd, 0L, 2);
    fp->fd = fd;
    fp->flag |= (Write | Read | Unbuf);
  }


  fp->fd = fd;
  fp->count = 0;
  fp->ptr = NULL;
  //printf("%d\n", file_count);

  //if in valid mode the fp->flag == 0
  //inthat case error: invalid mode
  if(!(fp->flag)){
      return NULL;
  }



return fp;



}

/*
int Fclose(File *fp){
  fp->ptr = fp->buffer;
  fp->count = 0;
  if(close(fp->fd) == -1)
  fp->flag = 0;
    return EOF;
  free(fp);
  int file_count;
  file_count --;
  return 0;

}
*/
