#include"File.h"

//initialising the stdI/O streams
File file_arr[3] = {
  {0, NULL, {'\0'}, Read, 0},
  {0, NULL, {'\0'}, Write | Unbuf, 1},
  {0, NULL, {'\0'}, Write | Unbuf, 2}
};


//clearbuf
void clearbuf(File *fp){

  if(fp->flag & Write){

    int buf_size;
    if(fp->flag & Unbuf)
      buf_size = 1;
    else
      buf_size = MAX_BUF;
    if(fp->ptr > fp->buffer)
      write(fp->fd, fp->buffer, (buf_size - fp->count));
    fp->ptr = NULL;
    fp->count = 0;
  }
}

//Fclose
int Fclose(File *fp){

  clearbuf(fp);

  if(close(fp->fd) == -1)
    return EOF;
  free(fp);
  file_count --;
  return 0;

}

//Fillbuf
void Fillbuf(File *fp){
    int buf_size;
    if(fp->flag & Unbuf)
      buf_size = 1;
    else
      buf_size = MAX_BUF;
  //printf("FILLBUF CALLED\n");
    fp->ptr = fp->buffer;
    fp->count = read(fp->fd, fp->buffer, buf_size);

    if(fp->count == 0)
      fp->flag |= Eof;

    if(fp->count < 0){
      fp->flag |= Err;
      fp->count = 0;
      return;
    }


}

//Flushbuf.c
void Flushbuf(File *fp){
  //  printf("Flushbuf CALLED\n");
    int flag, buf_size;
    if(fp->flag & Unbuf)
      buf_size = 1;
    else
      buf_size = MAX_BUF;
    if(fp->ptr != NULL){
    //  printf("fp->ptr != NULL *****\n");
      flag = write(fp->fd, fp->buffer, buf_size);
  //    printf("%d\n", flag);
      if(flag == -1){
      //  printf("error **********\n");
        fp->flag |= Err;
        return;
      }

    }

    fp->ptr = fp->buffer;
    fp->count = buf_size;
    return;



}

//Fopen
#define PERMS 0777 //read write execute for everyone

int file_count = 0;
//Fopen: opens the file and returns File pointer
File *Fopen(char *name, char *mode){
  File *fp;
  int fd;

  // handling the wrong mode condition
    //*******************************************
  //print invalid argument

//**************************************************
  //  finding free file slot in the file_arr
  /*for(fp = file_arr; fp < (file_arr + MAX_FILE); fp++){
    if(!(fp->flag)) // found a slot for the current file in file_arr
      break;
  }
*/

  if(file_count < MAX_FILE){
    fp = malloc(sizeof(File));
    if(fp == NULL)
      return NULL;
    file_count++;
    fp->ptr = NULL;
    fp->count = 0;
    fp->flag = 0;
  }


  //no free slots found in the file_arr
  //if(fp >= (file_arr + MAX_FILE)){
    //*************************************************
    //print error : max number of files openi

    //**************************************************
    //return NULL;
  //}

  //when mode is w
  if(!(strcmp(mode, "w"))){
    fd  = creat(name, PERMS);
    fd = open(name, O_WRONLY, PERMS);
    if(fd == -1)
      return NULL;
    fp->fd = fd;
    fp->flag |= Write;
  }

  //when mode is r
  if(!(strcmp(mode, "r"))){
    fd  = open(name, O_CREAT | O_RDONLY, PERMS);
    if(fd == -1)
      return NULL;
    fp->fd = fd;
    fp->flag |= Read;
  }


  //when mode is a
  if(!(strcmp(mode, "a"))){
    fd  = open(name, O_CREAT | O_WRONLY, PERMS);
    if(fd == -1)
      return NULL;
    lseek(fd, 0L, 2);
    fp->fd = fd;
    fp->flag |= Write;
  }

  //when mode is wb
  if(!(strcmp(mode, "wb"))){
    fd  = creat(name, PERMS);
    fd = open(name, O_WRONLY, PERMS);
    if(fd == -1)
      return NULL;
    fp->fd = fd;
    fp->flag |= Write;
  }

  //when mode is rb
  if(!(strcmp(mode, "rb"))){
    fd  = open(name, O_CREAT | O_RDONLY, PERMS);
    if(fd == -1)
      return NULL;
    fp->fd = fd;
    fp->flag |= Read;
  }


  //when mode is ab
  if(!(strcmp(mode, "ab"))){
    fd  = open(name, O_CREAT | O_WRONLY, PERMS);
    if(fd == -1)
      return NULL;
    lseek(fd, 0L, 2);
    fp->fd = fd;
    fp->flag |= Write;
  }



  //when mode is w+
  if(!(strcmp(mode, "w+"))){
    fd  = creat(name, PERMS);
    fd  = open(name, O_CREAT | O_RDWR, PERMS);


    if(fd == -1)
      return NULL;
    fp->fd = fd;
    fp->flag |= (Write | Read | Unbuf);

  }


  //when mode is r+
  if(!(strcmp(mode, "r+"))){
    fd  = open(name, O_CREAT | O_RDWR, PERMS);
    if(fd == -1)
      return NULL;
    fp->fd = fd;
    fp->flag |= (Write | Read | Unbuf);
  }

  //when mode is a+
  if(!(strcmp(mode, "a+"))){
    fd  = open(name, O_CREAT | O_RDWR, PERMS);
    if(fd == -1)
      return NULL;
    lseek(fd, 0L, 2);
    fp->fd = fd;
    fp->flag |= (Write | Read | Unbuf);
  }


  fp->fd = fd;
  fp->count = 0;
  fp->ptr = NULL;
  //printf("%d\n", file_count);

  //if in valid mode the fp->flag == 0
  //inthat case error: invalid mode
  if(!(fp->flag)){
      errno = EINVAL;
      return NULL;
  }



return fp;



}



//Fread
size_t Fread(void *ptr, size_t size, size_t nmemb, File *fp){
  char *temp;//to access the byte by byte memory pointed to by the ptr
  temp = ptr;
  size_t total = size * nmemb;
  size_t net_read = 0;
  if(!(fp->flag & Read)){
  //  printf("flag not read\n");
    return 0;//error invalid mode
  }

  if(fp->count == 0){
  //  printf("gone\n");
    Fillbuf(fp);
  }
  //if fp->count is zero after Fillbuf i.e EOF has occured
  if(fp->count == 0)
    return net_read;


  while((fp->count) && (net_read < total)){
    *temp = *(fp->ptr);
    temp++;
    fp->ptr++;
    fp->count--;
    net_read++;
    if(fp->count == 0){
      Fillbuf(fp);
    }
    if(fp->count == 0)
      return net_read;

  }

  return 0;

}


//Fseek
int Fseek(File *fp, long offset, int whence){
  if(whence == SEEK_SET){
    if(lseek(fp->fd, offset, 0) != -1){
      fp->ptr = NULL;
      fp->count = 0;
      return 0;
    }
    else{
      fp->flag |= Err;
      return -1;
    }
  }

  if(whence == SEEK_END){
    if(lseek(fp->fd, offset, 2) != -1){
      fp->ptr = NULL;
      fp->count = 0;
      return 0;
    }
    else{
      fp->flag |= Err;
      return -1;
    }
  }

  if(whence == SEEK_CUR){
    if(fp->flag & Write)
     clearbuf(fp);

   long x;
   x = lseek(fp->fd, 0L, 1 );
     //printf("this is the x from ftell %d\n", x);
   if(x > (MAX_BUF - 1)){


     if(lseek(fp->fd, offset - fp->count, 1) != -1){
       fp->ptr = NULL;
       fp->count = 0;
       return 0;
     }
     else{
       fp->flag |= Err;
       return -1;
     }

   }

   else{
     if(lseek(fp->fd, offset - fp->count, 1) != -1){
       fp->ptr = NULL;
       fp->count = 0;
       return 0;
     }
     else{
       fp->flag |= Err;
       return -1;
     }
   }


  }

  fp->flag |= Err;
  return -1;
 //   return (x  - fp->count );


}

//Ftell
long Ftell(File *fp){

  clearbuf(fp);

  long x;
  x = lseek(fp->fd, 0L, 1);
  //printf("this is the x from ftell %d\n", x);
  if(x > (MAX_BUF - 1)){
    //printf("x>max buf   %d\n", x);
    return (x  - fp->count );
  }
  return x - fp->count ;
}


//Fwrite
size_t Fwrite(void *ptr, size_t size, size_t nmemb, File *fp){
  char *temp;//to access the byte by byte memory pointed to by the ptr
  temp = ptr;
  size_t total = size * nmemb;
  size_t net_write = 0;
  if(!(fp->flag & Write)){

    return 0;//error invalid mode
  }

  if((fp->count == 0) || (fp->ptr == NULL)){
    Flushbuf(fp);
  }
  /*//if fp->count is zero after Fillbuf i.e EOF has occured
  if(fp->count == 0)
    return net_write;
*/

  while((fp->count) && (net_write < total)){
    *(fp->ptr) = *temp;
    temp++;
    fp->ptr++;
    fp->count--;
  //  printf("%d  fp->count\n", fp->count);
    net_write++;
    if((fp->count == 0) || (fp->ptr == NULL)){
      Flushbuf(fp);
    }
  //  if(fp->count == 0)
    //  return net_write;

  }

  return net_write;

}


//initfilearr
