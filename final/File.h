#ifndef File_H
#define File_H
#include<sys/types.h>
#include<sys/stat.h>
#include<stdlib.h>
#include<fcntl.h>
#include<errno.h>
#include<errno.h>
#include<stdio.h>
#include<string.h>
#include<unistd.h>
#define MAX_FILE 15 //max number of files open
#define MAX_BUF 100//max size of the buffer

//defining the File structure
typedef struct File{
  long int count; //number of characters left in the buffer
  char  *ptr; //pointer to the next character in the buffer
  char buffer[MAX_BUF]; //location of the buffer
  int flag; //mode of file access
  int fd; //file descriptor
}File;



//initialise the flags ie the modes
enum Flags {
  Read  =  01, //file opened for reading
  Write =  02, //file opend for writing
  Unbuf =  04, // file is unbeffered
  Eof   =  010,// EOF has occured
  Err   =  020// ERROR has occured
};




extern File file_arr[3] ; //array of standard file streams
extern int file_count ;//number of open files



//initialis stdin stdout and stderr to fd 0, 1, 2.
#define Stdin &(file_arr[0])
#define Stdout &(file_arr[1])
#define Stderr &(file_arr[2])


//macro feof()
#define Feof(fp) ((fp->flag & Eof) != 0)

//macro for Ferror()
#define Ferror(fp) ((fp->flag & Err) != 0)


//Fopen:opens the file with the <name> in the <mode>
//returns NULL if any error
extern File *Fopen(char *name, char *mode);


//fclose:closes the file and sets all the members of *fp to 0 or NULL
//Upon  successful  completion 0 is returned.  Otherwise, EOF is returned.
extern int Fclose(File *fp);

//initialise the standard i/o streams
extern void initfilearr();


//Fread
//returns the number of byte read.
size_t Fread(void *ptr, size_t size, size_t nmemb, File *fp);


//Fillbuf
//initialise or reinitialise the buffer
void Fillbuf(File *fp);

//Ftell
//returns the current offset, otherwise, -1 is returned.
long Ftell(File *fp);

//Fgetpos
#define Fgetpos(fp, ptr) (*ptr = (Ftell(fp)))

//Fwrite
//returns the number of bytes written.
size_t Fwrite(void *ptr, size_t size, size_t nmemb, File *fp);

//Flushbuf
void Flushbuf(File *fp);

//clearbuf
//clears the buffer into the fp
void clearbuf(File *fp);

//Fseek
int Fseek(File *fp, long offset, int whence);
#endif
