#include"File.h"
#include<stdio.h>
int main(){
  File *fp;
  char *s;
  s = malloc(sizeof(char) * 100);
  int c, i;
  printf("ENTER THE TEST NUMBER\n");
  scanf("%d", &c);
  while(c != -1){
    switch (c) {
      case 1:{

                    printf("***********************************************\n");
                    printf("WRITING 5 BYTES OF 'hello123' IN FILE1 OPENED IN 'w' MODE.\n");
                    fp = Fopen("FILE1", "w");
                    Fwrite("hello123", 5, 1, fp);
                    printf("CURRENT POSITION USING FTELL:  %ld\n", Ftell(fp));
                    Fclose(fp);
                    printf("STATUS: \n");
                    printf("***********************************************\n\n\n");
                    break;


      }
      case 2:{

                    printf("***********************************************\n");
                    printf("READING 5 BYTES IN STRING S FROM FILE1 OPENED IN 'R' MODE.\n");
                    fp = Fopen("FILE1", "r");
                    Fread(s, 5, 1, fp);
                    printf("CURRENT POSITION USING FTELL:  %ld\n", Ftell(fp));
                    printf("THIS IS THE STRING s: %s\n", s);
                    printf("STATUS: \n");
                    Fclose(fp);
                    printf("***********************************************\n\n\n");
                    break;

      }

      case 3:{

                    printf("***********************************************\n");
                    printf("APPENDING 123 TO TEXT IN FILE1 OPENED IN 'a' MODE.\n");
                    fp = Fopen("FILE1", "a");
                    Fwrite("123", 3, 1, fp);
                    printf("CURRENT POSITION USING FTELL:  %ld\n", Ftell(fp));
                    Fclose(fp);
                    fp = Fopen("FILE1", "r");
                    Fread(s, 100, 1, fp);
                    printf("THIS IS THE STRING s: %s\n", s);
                    printf("STATUS: \n");
                    Fclose(fp);
                    printf("***********************************************\n\n\n");
                    break;



      }

      case 4:{


                    printf("DOING ALL THE SAME THINGS USING WB, RB AND AB MODES\n\n");

                    printf("***********************************************\n");
                    printf("WRITING 5 BYTES OF 'helo123' IN FILE1 OPENED IN 'wb' MODE.\n");
                        fp = Fopen("FILE1", "wb");
                    Fwrite("hello123", 5, 1, fp);
                    printf("CURRENT POSITION USING FTELL:  %ld\n", Ftell(fp));
                    Fclose(fp);
                    printf("STATUS: \n");
                    printf("***********************************************\n\n\n");
                    break;

      }

      case 5:{

                    printf("***********************************************\n");
                    printf("READING 5 BYTES IN STRING S FROM FILE1 OPENED IN 'rb' MODE.\n");
                    fp = Fopen("FILE1", "rb");
                    free(s);
                    s = malloc(100);
                    Fread(s, 5, 1, fp);
                    s[5] = '\0';
                    printf("CURRENT POSITION USING FTELL:  %ld\n", Ftell(fp));
                    printf("__________________________________________________\n");
                    printf("THIS IS THE STRING s: %s\n", s);
                    printf("STATUS: \n");
                    printf("__________________________________________________\n");
                    Fclose(fp);
                    printf("***********************************************\n\n\n");
                    break;


      }

      case 6:{

                    printf("***********************************************\n");
                    printf("__________________________________________________\n");
                    printf("APPENDING 123 TO TEXT IN FILE1 OPENED IN 'ab' MODE.\n");
                    fp = Fopen("FILE1", "ab");
                    Fwrite("123", 3, 1, fp);
                    printf("CURRENT POSITION USING FTELL:  %ld\n", Ftell(fp));
                    printf("__________________________________________________\n");
                    Fclose(fp);
                    fp = Fopen("FILE1", "r");
                    Fread(s, 100, 1, fp);
                    printf("THIS IS THE STRING s: %s\n", s);
                    printf("STATUS: \n");
                    printf("__________________________________________________\n");
                    Fclose(fp);
                    printf("***************************************************************************\n\n\n");
                    break;

      }

      case 7:{

                    printf("***************************************************************************\n\n\n");
                    printf("OPENING FILE3 IN W+ MODE\n");
                    fp = Fopen("FILE3", "w+");
                    printf("__________________________________________________\n");
                    printf("WRITING 'hello' IN FILE3\n");
                    Fwrite("hello", 5, 1, fp);
                    printf("__________________________________________________\n");
                    printf("CURRENT POSITIION:   %ld\n", Ftell(fp));
                    printf("__________________________________________________\n");
                    printf("FSEEKING TO POSITION 2\n");
                    Fseek(fp, -3, 1);
                    printf("CURRENT POSITON:   %ld\n", Ftell(fp));
                    printf("__________________________________________________\n");
                    printf("READING REMAINING BYTES FROM THE CURRENT POSITION\n");
                    free(s);
                    s = malloc(sizeof(char) * 100);
                    Fread(s, 100, 1, fp);
                    printf("TEXT FROM PPOSITION TWO IS:   %s\n", s);
                    printf("__________________________________________________\n");
                    printf("FSEEKING THE END POSITION\n");
                    Fseek(fp, 0, 2);
                    printf("CURRENT POSITION:   %ld\n", Ftell(fp));
                    printf("__________________________________________________\n");
                    printf("WRITING '123' TO THE FILE FROM THE END POSITION\n");
                    Fwrite("123", 3, 1, fp);
                    printf("__________________________________________________\n");
                    printf("FSEEKING TO HE START OF THE FILE3\n");
                    Fseek(fp, 0, 0);
                    printf("READING THE ENTIRE FILE3 IN SRTING S\n");
                    printf("CURRENT POSITION:   %ld\n", Ftell(fp));
                    free(s);
                    s = malloc(sizeof(char) * 100);
                    Fread(s, 8, 1, fp);
                    s[8] = '\0';
                    printf("THE SRTING S IS:   %s\n", s);
                    printf("__________________________________________________\n");
                    free(s);
                    Fclose(fp);
                    printf("***************************************************************************\n\n\n");
                    break;


      }

      case 8:{

                    printf("DOING THE SAME THINGS IN R+ MODE IN FILE4\n");
                    printf("***************************************************************************\n\n\n");
                    printf("OPENING FILE4 IN r+ MODE\n");
                    fp = Fopen("FILE4", "r+");
                    printf("__________________________________________________\n");
                    printf("WRITING 'hello' IN FILE4\n");
                    Fwrite("hello", 5, 1, fp);
                    printf("__________________________________________________\n");
                    printf("CURRENT POSITIION:   %ld\n", Ftell(fp));
                    printf("__________________________________________________\n");
                    printf("FSEEKING TO POSITION 2\n");
                    Fseek(fp, -3, 1);
                    printf("CURRENT POSITON:   %ld\n", Ftell(fp));
                    printf("__________________________________________________\n");
                    printf("READING 2 BYTES FROM THE CURRENT POSITION\n");
                    free(s);
                    s = malloc(sizeof(char) * 100);
                    Fread(s, 100, 1, fp);
                    printf("TEXT FROM PPOSITION TWO IS:   %s\n", s);
                    printf("__________________________________________________\n");
                    printf("FSEEKING THE END POSITION\n");
                    Fseek(fp, 0, 2);
                    printf("CURRENT POSITION:   %ld\n", Ftell(fp));
                    printf("__________________________________________________\n");
                    printf("WRITING '123' TO THE FILE FROM THE END POSITION\n");
                    Fwrite("123", 3, 1, fp);
                    printf("__________________________________________________\n");
                    printf("FSEEKING TO HE START OF THE FILE4\n");
                    Fseek(fp, 0, 0);
                    printf("READING THE ENTIRE FILE3 IN SRTING S\n");
                    printf("CURRENT POSITION:   %ld\n", Ftell(fp));
                    free(s);
                    s = malloc(sizeof(char) * 100);
                    Fread(s, 8, 1, fp);
                    s[8] = '\0';
                    printf("THE SRTING S IS:   %s\n", s);
                    printf("__________________________________________________\n");
                    Fclose(fp);
                    printf("***************************************************************************\n\n\n");
                    break;

      }

      case 9:{

                    printf("***************************************************************************\n");
                    printf("OPENING FILE3 IN A+ MODE\n");
                    fp = Fopen("FILE3", "a+");
                    printf("CURRENT POSITION:   %ld\n", Ftell(fp));
                    printf("__________________________________________________\n");
                    printf("APPENDING '456' TO THE TEXT IN THE FILE3\n");
                    Fwrite("456", 3, 1, fp);
                    printf("CURRENT POSITION:   %ld\n", Ftell(fp));
                    printf("__________________________________________________\n");
                    printf("FSEEKING TO THE START OF FILE3 \n");
                    Fseek(fp, 0, 0);
                    printf("READING THE CONTENTS OF THE FILE3 IN S\n");
                    free(s);
                    s = malloc(sizeof(char) * 100);
                    Fread(s, 11, 1, fp);
                    s[11] = '\0';
                    printf("THE STRING S IS:   %s\n", s);
                    printf("__________________________________________________\n");
                    printf("CURRENT POSITON:   %ld\n", Ftell(fp));
                    printf("__________________________________________________\n");
                    Fclose(fp);
                    printf("***************************************************************************\n\n\n");
                    break;


      }

      case 10:{

                    printf("TESTTING THE STANDARD I/O STREAMS\n");
                    printf("***************************************************************************\n");
                    printf("ENTER THE STRING TO BE PRINTED TO STDOUT:  \n");
                    free(s);
                    s = malloc(sizeof(char) * 100);
                    int i = 0;
                    while((s[i] = getchar()) != -1)
                      i++;
                    s[i] = '\0';
                    printf("\n");
                    Fwrite(s, i, 1, Stdout);
                    printf("\n");
                    printf("***************************************************************************\n\n\n");
                    break;

      }

      case 11:{




                    printf("TESTING STDIN\n");
                    printf("***************************************************************************\n");
                    printf("ENTER AN 2 DIGIT INTEGER:   \n");
                    free(s);
                    s = malloc(100);
                    Fread(s, 2, 1, Stdin);
                    s[2] = '\0';
                    i = atoi(s);
                    printf("\n");
                    printf("INTEGER + 10 = %d\n", i + 10);
                    printf("***************************************************************************\n\n\n");
                    break;





      }

      case 12:{
        break;

      }
      default:
        printf("ENTER THE CORRECT TEST NUMBER\n");
        break;
    }

    printf("ENTER THE TEST NUMBER\n");
    scanf("%d", &c);

  }




















}
